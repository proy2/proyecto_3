import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from desarolladoras import GtkDialog
from proyecto import hipotesis_alcohol , hipotesis_azucarvsobesidad
from proyecto import hipotesis_obesidadvsmuertes , hipotesis_top5_obesidad    
from proyecto import hipotesis_top10_alcoholVSconfirmados

class MainWindow():
     def __init__(self):
         self. builder = Gtk.Builder()
         self.builder.add_from_file("ventana_principal.glade")
         self.window = self.builder.get_object("ventana_window")
         self.window.connect("destroy", Gtk.main_quit)
         self.window.set_title("Estadisticas Dumbo")
         self.window.resize(800, 600)


        # Se crea una lista con una sola columna
         self.lista_variables = Gtk.ListStore(str)
         cell = Gtk.CellRendererText()
         self.combobox = self.builder.get_object("combox")
         self.combobox.connect("changed", self.conseguir_nombre)

         self.boton_creadoras = self.builder.get_object("aceptar")
         self.boton_creadoras.connect("clicked", self.abrir_dialogo)
         
         self.image = self.builder.get_object("Imagen") 
         self.labe = self.builder.get_object("label")
         self.label = self.builder.get_object("label2") 
         self.combobox.pack_start(cell, True)
         self.combobox.add_attribute(cell, "text", 0)
 
         Nombres = ["¿Cuál es el país con el mayor consumo de alcohol?",
                    "¿Existe una correlación entre los países con altos porcentajes de azúcar y la obesidad?", 
                    "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?",
                    "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?",
                    "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?"]

        # Se pasan los nombres de uno en uno
         for nombre in Nombres:
             self.lista_variables.append([nombre])

         self.combobox.set_model(model=self.lista_variables)
         self.window.show_all()


     def conseguir_nombre(self, btn=None):

         modelo = self.combobox.get_model()
         indice = self.combobox.get_active_iter()
         nombre = modelo[indice][0]

         if nombre == "¿Cuál es el país con el mayor consumo de alcohol?":
             alcohol = hipotesis_alcohol()
             self.labe.set_text(f"{alcohol}")  
             self.label.set_text(" ")
             self.image.set_from_file("Alcohol_mayor.png")
             self.add(self.image)
 
         elif nombre == "¿Existe una correlación entre los países con altos porcentajes de azúcar y la obesidad?":
             (obesidad,azucar)=hipotesis_azucarvsobesidad()
             self.labe.set_text(f"{obesidad}")
             self.label.set_text(f"{azucar} ")    
             self.image.set_from_file("grafico_comparacion_obesidadvsazucar.png")
             self.add(self.image) 
         elif nombre == "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?":
             (obesidad,muertes)=hipotesis_obesidadvsmuertes()
             self.labe.set_text(f"{obesidad}")
             self.label.set_text(f"{muertes} ") 
             self.image.set_from_file("grafico_comparacion_obesidadVSmuertes.png")                                  
             self.add(self.image)      
         elif nombre == "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?":
             obeso = hipotesis_top5_obesidad()
             self.labe.set_text(f"{obeso} ")
             self.label.set_text(" ")
             self.image.set_from_file("grafico_top5_obesidad.png")
             self.add(self.image)      

         else:
             (confirmados,alcohol)=hipotesis_top10_alcoholVSconfirmados()
             self.labe.set_text(f"{confirmados}")
             self.label.set_text(f"{alcohol} ") 
             self.image.set_from_file("grafico_proy.png")
             self.add(self.image)      


        

     def abrir_dialogo(self, btn=None):
         dlg = GtkDialog()
         response = dlg.dialogo.run()
    




if __name__ == "__main__":
    MainWindow()
    Gtk.main()


