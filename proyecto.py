import pandas as pan                                                                                                               
import matplotlib.pyplot as plt

COMIDA = "suministro_alimentos_kcal.csv"

def abrir():
    archivo = pan.read_csv(COMIDA)
    return archivo
def remover_alcohol(data):
    new_data = data[['Alcoholic Beverages', 'Country']]
    return new_data

def remover_2(data):
    new_data = data[['Sugar & Sweeteners','Country']]
    return new_data 
def remover_1(archivo):
    archivo_2 = archivo[['Obesity','Country']]
    return archivo_2
def reducir(archivo):
    archivo_new = archivo[['Obesity','Country','Population']]
    return archivo_new 
def reducir_2(data):
    archivo_new = data[['Deaths','Country','Population']]
    return archivo_new
def pasar_a_enteros(archivo):
    archivo["Obesity"] = archivo["Obesity"] / 100
    archivo["Obesity real"] = archivo["Obesity"] * archivo["Population"]
    archivo.drop(['Population','Obesity'],
                  axis=1,
                  inplace=True)
    return archivo
def pasar_a_enteros2(data):
    data["Deaths"] = data["Deaths"] / 100
    data["Deaths real"] = data["Deaths"] * data["Population"]   
    data.drop(['Population','Deaths'],
            axis=1,
            inplace=True)
    return data

def mayor_obesidad_enteros(archivo):
    obesidad = 0
    mayor_o = 0
    max_o = None
    for num in range(170):
        obesidad = archivo["Obesity real"][num].astype(type(float)) 
        if obesidad > mayor_o:
            mayor_o = obesidad
            max_o = archivo["Country"][num]
    obesidad = archivo[archivo['Obesity real'] >= 37710381]
    print(obesidad,"\n")
    return (obesidad , max_o)

def mayor_obesidad(archivo):
    obesidad = 0
    mayor_o = 0
    max_o = None
    for num in range(170):
        obesidad = archivo["Obesity"][num].astype(type(float))
        if obesidad > mayor_o:
            mayor_o = obesidad
            max_o = archivo["Country"][num]
    obesidad = archivo.query("31.3 < Obesity <= 45.6")
    print(obesidad)
    return (obesidad , max_o)

def mayor_azucar(data):
    azucar = 0
    mayor_a = 0
    max_a = None
    for num in range(170):
        azucar = data["Sugar & Sweeteners"][num].astype(type(float))
        if azucar > mayor_a:
            mayor_a = azucar
            max_a = data["Country"][num]
    azucar = data[data['Sugar & Sweeteners'] >= 7.95]
    print(azucar)
    return (azucar , max_a)

def mayor_alcohol(data):
    alcohol = 0
    mayor_al = 0
    max_al = None
    for num in range(170):
        alcohol = data["Alcoholic Beverages"][num].astype(type(float))
        if alcohol > mayor_al:
            mayor_al = alcohol
            max_al = data["Country"][num]
    rango = data[data['Alcoholic Beverages'] >= 3]
    print(f"El país con mayor porcentaje de consumo de alcohol es {max_al} con {mayor_al}")
    return rango

def mayor_muertes(data):
    muertes = 0
    mayor_m = 0
    max_m = None
    for num in range(170):
        muertes = data["Deaths real"][num].astype(type(float))
        if muertes > mayor_m:
            mayor_m = muertes
            max_m = data["Country"][num]
    muertes = data[data['Deaths real'] >= 46000]
    print(muertes)
    return (muertes , max_m)

def imprimir_resultado_comparar(archivo, data):
    print("\nComparamos los países con los mayores porcentajes de cada uno: ",
          f"\nEl pais con el mayor porcentaje de obesidad es {archivo} y porcentaje de azúcar es {data}")

def imprimir_resultado_compara(archivo, data):
    print("\nComparamos los paises mayores porcentajes de cada uno:",
          f"\nEl país con el mayor porcentaje de obesidad es {archivo} y porcentaje de muertes es {data}")
    
def grafico_alcohol(data):
    data.plot(kind="bar",x="Country")
    plt.savefig("Alcohol_mayor.png")

def grafico_comparar(archivo, data):
    fig, ax2 = plt.subplots(nrows=2, ncols=1)
    data.plot(kind="bar",x="Country", ax=ax2[0])
    archivo.plot(kind="bar",x="Country", ax=ax2[1])
    plt.savefig("grafico_comparacion_obesidadvsazucar.png")

def grafico_comparar2(archivo, data):
    fig, ax2 = plt.subplots(nrows=2, ncols=1)
    data.plot(kind="bar",x="Country", ax=ax2[0])
    archivo.plot(kind="bar",x="Country", ax=ax2[1])
    plt.savefig("grafico_comparacion_obesidadVSmuertes.png")

def crea_columna_habitantes_obesos(data):
    data["Obese Population"] = (data["Obesity"] * data["Population"]) / 100
    return data

def top5(new_data):
    top5 = new_data.sort_values("Obese Population",ascending=False)
    print(top5.head(5))
    new_data2 = new_data[["Country", "Population", "Obese Population"]]
    top5datos = new_data2.sort_values("Obese Population",ascending=False)
    datos_grafico = top5datos.head(5).copy()
    datos_grafico.plot(kind="bar", x="Country", figsize=(5,7))
    plt.savefig("grafico_top5_obesidad.png")
    return top5


def hipotesis_top5_obesidad():
    data = abrir()
    crea_columna_habitantes_obesos(data)
    new_data = data[["Country", "Obesity", "Population", "Obese Population"]]
    print("\nLos 5 países con el mayor número de habitantes obesos con respecto al número de su población son:\n")
    print("Estados Unidos, China, India, Brasil y Rusia.\n")
    new_data =top5(new_data)
    new_data.drop(['Population','Obesity'],
                  axis=1,
                  inplace=True)
    return new_data.head(5)

def nuevo_data(data):
    new_data = data[["Country", "Alcoholic Beverages", "Confirmed"]]
    return new_data

def consumo_alcohol(data):
    alcohol = 0
    mayor_al = 0
    max_al  = None
    for num in range(170):
        alcohol = data["Alcoholic Beverages"][num].astype(type(float))
        if alcohol > mayor_al:
            mayor_al = alcohol
            max_al= data["Country"][num]
            
    rango = data[data['Alcoholic Beverages'] >= 3]
  #  print(f"El pais con el mayor consumo de alcohol es {max_al} con un porcentaje de {mayor_al}")
    return rango 

def top_alcohol(data):

    dtf = pan.DataFrame({'Alcoholic Beverages': [5.1574, 4.8993, 3.8856,
                                               3.6782, 3.3192, 3.2979,
                                               3.2902, 3.2284, 3.0614,
                                               3.0432]},

                  index=["Luxembourg", "Czechia ", "Bosnia and Herzegovina ",
                         "Bulgaria ", "Lithuania", "Belarus ", "Germany ",
                         "Ireland ", "Latvia", "Hungary"])

    print(dtf)
    return dtf 

def casos_confirmados(data):
    confirmados = 0
    mayor_conf = 0
    max_conf  = None
    for num in range(170):
        confirmados = data["Confirmed"][num].astype(type(float))
        if confirmados > mayor_conf:
            mayor_conf = confirmados
            max_conf= data["Country"][num]

    rango = data[data['Confirmed'] >= 3]
    print(f"El país con el mayor número de casos confirmados de COVID es {max_conf} con un porcentaje de {mayor_conf}")
    return rango

def top_confirmados(data):

    df = pan.DataFrame({'Confirmed': [3.88497611810682, 3.42486979166667, 
                                     3.27450541246734, 3.16129905277402,
                                     3.15001167406024, 3.08842443729904, 
                                     3.02231012658228, 2.8165221688018,
                                     2.74952169144528, 2.71355787678533]},

                  index=["Belgium", "Israel", "Czechia",
                         "Armenia", "Panama ", "Montenegro", "Luxembourg", 
                         "United States of America", "Peru ", "Kuwait"])
    print(df)
    return df 

def grafico(data):
    data = data[["Country", "Confirmed", "Alcoholic Beverages"]].copy()
    top_con = data[["Confirmed", "Alcoholic Beverages"]].copy()
    top_al = data[["Confirmed", "Alcoholic Beverages"]].copy()

    top_con.sort_values(by="Confirmed", inplace=True, ascending=False)
    top_al.sort_values(by="Alcoholic Beverages", inplace=True, ascending=False)
    top1 = top_con.head(10).copy()
    top2 = top_al.head(10).copy()
    ax = top1.plot(kind="scatter", x="Confirmed", y="Alcoholic Beverages",
            colOr="Red", label="Top Confirmados")
    top2.plot(kind="scatter", x="Confirmed", y="Alcoholic Beverages",
            color="Blue", label="Top Alcoholicos", ax=ax)
    plt.savefig("grafico_proy.png")
    
def hipotesis_top10_alcoholVSconfirmados():
    data = abrir()
    archivo_editado = nuevo_data(data)
    mayor_alcohol = consumo_alcohol(data)
    mayor_confirmados = casos_confirmados(data)
    lista_confirmados = top_confirmados(data)
    lista_alcohol = top_alcohol(data)
    scatter_grafico = grafico(data)
    return( lista_confirmados,lista_alcohol)

def hipotesis_azucarvsobesidad():
    archivo = abrir()
    data = abrir()
    data = remover_2(data)
    archivo = remover_1(archivo)
    (obesidad,max_o) = mayor_obesidad(archivo)
    (azucar,max_a) = mayor_azucar(data)
    imprimir_resultado_comparar(max_o, max_a)
    grafico_comparar(obesidad, azucar)
    return (obesidad,azucar)

def hipotesis_alcohol():
    archivo = abrir()
    archivo = remover_alcohol(archivo)
    alcohol = mayor_alcohol(archivo)
    grafico = grafico_alcohol(alcohol)
    return alcohol

def hipotesis_obesidadvsmuertes():
    archivo = abrir()
    data = abrir()
    archivo = reducir(archivo)
    data = reducir_2(data)
    archivo = pasar_a_enteros(archivo)
    data = pasar_a_enteros2(data)
    (obesidad,max_o)= mayor_obesidad_enteros(archivo)
    (muertes,max_m) = mayor_muertes(data)
    imprimir_resultado_compara(max_o, max_m)
    grafico_comparar2(obesidad, muertes) 
    return (obesidad,muertes)
def main():
    if __name__ == "__main__":                                                                                      
        main() 
